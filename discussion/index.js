// Conditional Statements
// allows us to control the flow of our program


/*
Syntax:

	if(condition){
		statement;
	}




*/

// [SECTION] if, else if, else Statement

//  if Statement

let numA = 5;

if (numA < 3) {
	console.log('Hello');
}
else{
	alert('Wrong hole');
}


let city = "New York";

if(city === "New York"){
	console.log("Welcome to New York City!");
}


// else if Clause

/*
Executes a statement if previous condition are false and if the  specified condition is true.

The "else if" clause is optional and can be added to capture additional conditions to change the flow of the program..

*/




let numB = 1;


if(numA > 3){
	console.log("Hi!");
} else if (numB > 0){
	console.log('World');
};

// =======================================

if(numA < 3){
	console.log("Hi!");
} else if (numB > 0){
	console.log('World');
};


// else if in string ============

let city1 = "Tokyo";

if(city1 === "New York"){
	console.log("Welcome to New York City!")
}else if(city1 === "Tokyo"){
	console.log("Welcome to Tokyo Japan!");
};



// else Statement

let numC = -5;
let numD = 7;

if(numC > 0) {
	console.log('AY');
}else if(numD === 0){
	console.log("yayaya");
}else{
	console.log('Error!');
}



// if, else if and else statements with funcitons


function determineTyphoonIntesity(windSpeed){
	if(windSpeed < 30){
		return 'Not a typhoon yet.';
	} else if (windSpeed <= 61){
		return 'Tropical Depression detected.';
	}else if(windSpeed >= 62 && windSpeed <= 88){
		return 'Tropical storm detected';
	} else if (windSpeed == 89 || windSpeed<= 117){
		return 'severe Tropical Storm detected';
	}
	else{
		return 'Typhoon Detected.';
	}
}


let foreCast = determineTyphoonIntesity(88);
console.log(foreCast);



if (foreCast == 'Tropical storm detected'){
	console.warn("Be Ready to leave the area!");
}


// [SECTION] conditional (Ternary) Operator

/*
	The conditional (Ternary) Operator takes in three operands:
	1. condition
	2. expression to execute if the condition is truthy
	3. expression to execute if the condition is falsy

   Ternary operator is for shorthand code - commonly for single statement execution where the result consists of only one line of code.

	Syntax
		(expression) ? true : false


		Can be used as an alternative to an "if else" statement
*/



// Single statement execution



let ternaryResult = (1 < 18) ? true : false
console.log("Result of Ternary Operator: " + ternaryResult);


// Multiple Statement Execution
let name;

function isOfLegalAge() {
    name = 'John';
    return 'You are of the legal age limit';
}

function isUnderAge() {
    name = 'Jane';
    return 'You are under the age limit';
}


// the parseInt function converts the input receive into a number datatype.

let age = parseInt(prompt("What is your age?"));
console.log(age);
let legalAge = (age >= 18) ? isOfLegalAge() : isUnderAge();
console.log("Result of Ternary Operator in functions: " + legalAge + ', ' + name);

// [SECTION] Switch Statement

/*
	the swithc statements evaluates an expression and matches the expression's value to a case clause. the swtich will then execute the statements associated with that case, as well as statements in cases that follow the matching case.



	switch(expression){
		case value:
			statement;
			break;
		default: statement;

	}

*/

let day = prompt("What day is it today?").toLowerCase();
console.log(day);


switch (day) {
    case 'monday': 
        console.log("The color of the day is red");
        break;
    case 'tuesday':
        console.log("The color of the day is orange");
        break;
    case 'wednesday':
        console.log("The color of the day is yellow");
        break;
    case 'thursday':
        console.log("The color of the day is green");
        break;
    case 'friday':
        console.log("The color of the day is blue");
        break;
    case 'saturday':
        console.log("The color of the day is indigo");
        break;
    case 'sunday':
        console.log("The color of the day is violet");
        break;
    default:
        console.log("Please input a valid day");
        break;
}